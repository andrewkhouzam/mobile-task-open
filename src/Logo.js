import Ionicons from 'react-native-vector-icons/Ionicons';
import React, {Component} from 'react';
import {ActivityIndicator, Alert, StyleSheet, Text, View, ImageBackground , ScrollView, TextInput, Button, Image} from 'react-native';
import { List, ListItem } from 'react-native-elements';
import axios from 'axios';
import { StackNavigator, TabNavigator  , TabBarBottom} from 'react-navigation';

class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('../assets/logo.png')}
        style={{ width: 60, height: 43 }}
      />
    );
  }
}

export default LogoTitle
