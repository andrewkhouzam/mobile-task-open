import React, {Component} from 'react';
import {ActivityIndicator, Alert, StyleSheet, Text, View, ImageBackground , ScrollView, TextInput, Button, Image} from 'react-native';
import { List, ListItem } from 'react-native-elements';
import axios from 'axios';
import { StackNavigator, TabNavigator  , TabBarBottom} from 'react-navigation';
import LogoTitle from './Logo';
import Search from 'react-native-search-box';


class HomeScreen extends React.Component {
  static navigationOptions= {
    headerTitle: <LogoTitle />,
    title: "Stars"
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      list: [],
      prevlist: [],
      url: 'https://swapi.co/api/people',
    }
  }

  componentWillMount() {
    return axios.get(this.state.url)
      .then((response) => {
        this.setState({
          isLoading: false,
          list: response.data.results,
          received: true,
        })
      })
      .catch((error) => {
        console.error(error);
      });
  }
  componentWillUpdate(){
    return axios.get(this.state.url)
      .then((response) => {
        this.setState({
          isLoading: false,
          list:response.data.results,
          received: true,
        })
      })
      .catch((error) => {
        console.error(error);
      });
    }
  render() {
    if (this.state.isLoading) {
     return (
       <View style={{flex: 1, paddingTop: 20}}>
         <ActivityIndicator style={{flex: 1}}/>
       </View>
     );
   }

    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (
      <View style={{flex: 1}}>
      <Search

        onChangeText={(text) => {
          this.setState({
            received: false,
            url: 'https://swapi.co/api/people/?search='+ text,
          });
          }
       }
        beforeCancel={() => {
          this.setState({
            received: false,
            url: 'https://swapi.co/api/people',
          });

          }
       }
      />
      { this.state.received?
      <ScrollView style={styles.scroll}>
        <List containerStyle={{marginBottom: 20}} style={{flex: 1}}>
            {
              this.state.list.map((l, i) => (
                <ListItem
                  roundAvatar
                  avatar={{uri:pic.uri}}
                  key={i}
                  title={l.name}
                  onPress={() => this.props.navigation.navigate('Details', {
                    data: l
                  })
                }
                />
              ))
            }
          </List>
      </ScrollView>
    :
    <ActivityIndicator style={{flex: 1}}/>
  }
      </View>
    );
  }
}



class Greeting extends Component {
  render() {
    return (
      <Text>Hello {this.props.name}!</Text>
    );
  }
}

class LotsOfGreetings extends Component {
  render() {
    return (
      <View style={{alignItems: 'center'}}>
        <Greeting name='Rexxar' />
        <Greeting name='Jaina' />
        <Greeting name='Valeera' />
      </View>
    );
  }
}

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {isShowingText: true};

    // Toggle the state every second
    setInterval(() => {
      this.setState(previousState => {
        return { isShowingText: !previousState.isShowingText };
      });
    }, 1000);
  }

  render() {
    let display = this.state.isShowingText ? this.props.text : ' ';
    return (
      <Text>{display}</Text>
    );
  }
}

class BlinkApp extends Component {
  render() {
    return (
      <View>
        <Blink text='I love to blink' />
        <Blink text='Yes blinking is so great' />
        <Text>Why did they ever take this out of HTML</Text>
        <Text>Look at me look at me look at me</Text>
      </View>
    );
  }
}

class FixedDimensionsBasics extends Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
        <View style={{width: 100, height: 100, backgroundColor: 'skyblue'}} />
        <View style={{width: 150, height: 150, backgroundColor: 'steelblue'}} />
      </View>
    );
  }
}

class FlexDimensionsBasics extends Component {
  render() {
    return (
      // Try removing the `flex: 1` on the parent View.
      // The parent will not have dimensions, so the children can't expand.
      // What if you add `height: 300` instead of `flex: 1`?
      <View style={{flex: 1}}>
        <View style={{flex: 1, backgroundColor: 'powderblue'}} />
        <View style={{flex: 2, backgroundColor: 'skyblue'}} />
        <View style={{flex: 3, backgroundColor: 'steelblue'}} />
      </View>
    );
  }
}

class PizzaTranslator extends Component {
  constructor(props) {
    super(props);
    this.state = {text: ''};
  }

  render() {
    return (
      <View style={{padding: 10}}>
        <TextInput
          style={{height: 40}}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({text})}
        />
        <Text style={{padding: 10, fontSize: 42}}>
          {this.state.text.split(' ').map((word) => word && '🍕').join(' ')}
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scroll: {
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  }
});


export default HomeScreen
