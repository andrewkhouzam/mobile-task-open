import React, {Component} from 'react';
import {ActivityIndicator, Alert, StyleSheet, Text, View, ImageBackground , ScrollView, TextInput, Button, Image} from 'react-native';
import { List, ListItem } from 'react-native-elements';
import axios from 'axios';
import { StackNavigator, TabNavigator  , TabBarBottom} from 'react-navigation';
import LogoTitle from './Logo';

class DetailsScreen extends React.Component {
  static navigationOptions= {
    title: "Details",
    headerTitle: <LogoTitle />,
  };

  render(){
     const { data } = this.props.navigation.state.params;
    let pic = {
      uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    return (
        <View>
          <Text>{data.name}</Text>
          <Text>Eye Color: {data.eye_color}</Text>
          <ImageBackground source={pic} style={{width: 375, height: 200}}>
            <Text>Hello World</Text>
          </ImageBackground>
        </View>
    )}

}

export default DetailsScreen
