import Ionicons from 'react-native-vector-icons/Ionicons';
import React, {Component} from 'react';
import {ActivityIndicator, Alert, StyleSheet, Text, View, ImageBackground , ScrollView, TextInput, Button, Image} from 'react-native';
import { List, ListItem } from 'react-native-elements';
import axios from 'axios';
import { StackNavigator, TabNavigator  , TabBarBottom} from 'react-navigation';
import HomeScreen from './src/Home';
import LogoTitle from './src/Logo';
import DetailsScreen from './src/Details';
import SettingsScreen from './src/Settings'
import FilmsScreen from './src/Films'
import OthersScreen from './src/Others'


const HomeStack = StackNavigator({
  Home: {
    screen: HomeScreen,
  },
  Details: {
     screen: DetailsScreen,
   },
  Settings: {
      screen: SettingsScreen,
  },
},
  {
    // initialRouteName: 'Home',
    navigationOptions: {
            headerStyle : {
                backgroundColor: 'white',
                height: 60
            },
            headerTintColor: 'black',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
  }
);


const SettingsStack = StackNavigator({
  Settings: {
     screen: SettingsScreen,
   },
  Home: {
    screen: HomeScreen,
  },
  Details: {
     screen: DetailsScreen,
   },

},
  {
    // initialRouteName: 'Settings',
    navigationOptions: {
            headerStyle : {
                backgroundColor: 'white',
                height: 60
            },
            headerTintColor: 'black',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
  }
);


const FilmsStack = StackNavigator({
  Films: {
     screen: FilmsScreen,
   },
  Details: {
     screen: DetailsScreen,
   },

},
  {
    // initialRouteName: 'Settings',
    navigationOptions: {
            headerStyle : {
                backgroundColor: 'white',
                height: 60
            },
            headerTintColor: 'black',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
  }
);

const OthersStack = StackNavigator({
  Others: {
     screen: OthersScreen,
   },
  Details: {
     screen: DetailsScreen,
   },

},
  {
    // initialRouteName: 'Settings',
    navigationOptions: {
            headerStyle : {
                backgroundColor: 'white',
                height: 60
            },
            headerTintColor: 'black',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
  }
);


export default TabNavigator({
  First: { screen: HomeStack },
  Second: { screen: FilmsStack },
  Third: { screen: SettingsStack },
  Fourth: { screen: SettingsStack },
  Fifth: { screen: OthersStack },

},
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'First') {
          iconName = `ios-home${focused ? '' : '-outline'}`;
        } else if (routeName === 'Second') {
          iconName = `ios-help-buoy${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Third'){
          iconName = `ios-infinite${focused ? '' : '-outline'}`;
        }
        else if (routeName === 'Fourth'){
          iconName = `ios-jet${focused ? '' : '-outline'}`;
        }
        else {
          iconName = `ios-options${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    },
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
  }
);

// export default TabNavigator({
//   Home: { screen: HomeScreen },
//   Settings: { screen: SettingsScreen },
// });
